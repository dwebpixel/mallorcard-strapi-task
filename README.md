# Strapi application

A sample Strapi Task. 

Database dump is in folder db-dump

## Credentials

#### Strapi
Username: Durgesh

Password: qwertyuiop

#### Database
Username: root

Password: dweb

#### Install a locally created provider to send emails
```
npm install ./providers/strapi-provider-email-durgeshmailer --save
```

#### Sample POST request JSON to add new activity
```javascript
{
	"title": "Motorcycle Touring",
    "subtitle": "Love of traveling and motorbike riding testing",
    "description": "Touring India on a motorcycle is the ultimate way of exploring the countryside if you have a love of traveling and motorbike riding. Enjoy the freedom to discover and experience India in a way which isn't possible using other forms of transport.",
    "price": 3000,
    "german_translation": {
        	"title": "Touring en moto",
            "subtitle": "Liebe zum Reisen und Motorradfahren",
            "description": "Eine Motorradtour durch Indien ist die ultimative Art, die Landschaft zu erkunden, wenn Sie gerne reisen und Motorrad fahren. Genießen Sie die Freiheit, Indien auf eine Weise zu entdecken und zu erleben, die mit anderen Verkehrsmitteln nicht möglich ist."
        },
    "spanish_translation": {
        "title": "Trekking y Montañismo",
        "subtitle": "Amor por viajar y andar en moto",
        "description": "Viajar por la India en motocicleta es la mejor manera de explorar el campo si te encanta viajar y andar en moto. Disfrute de la libertad de descubrir y experimentar la India de una manera que no es posible con otras formas de transporte."
    }
}
```

#### Sample PUT request JSON to apply discount
```javascript
{
	"Discount": 10
}
```
