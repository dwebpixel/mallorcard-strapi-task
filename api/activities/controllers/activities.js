"use strict";

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

const { parseMultipartData, sanitizeEntity } = require("strapi-utils");

module.exports = {
  /**
   * Create a record.
   *
   * @return {Object}
   */

  async create(ctx) {
    let entity;
    if (ctx.is("multipart")) {
        const { data, files } = parseMultipartData(ctx);
        entity = await strapi.services.activities.create(data, { files });
    } else {
        entity = await strapi.services.activities.create(ctx.request.body);
    }

    let activity = sanitizeEntity(entity, { model: strapi.models.activities });

    //Send emails to admin
    await strapi.plugins["email"].services.email.send({
      to: "info@mallorcard.es,dwebsoftguy@gmail.com",
      from: "Durgesh <test@durgeshtayade.com>",
      subject: "New activity created!",
      text: `New activity created - ${activity.title}`,
      html: `New activity created - ${activity.title}`,
    });

    return activity;
  },

  /**
   * Update Price using Discount a record.
   *
   * @return {Object}
   */

  async updatePriceWithDiscount(ctx) {
    //Get discount from a query variable or a JSON body
    const discount =
      Number(ctx.query.discount) || Number(ctx.request.body.Discount);

    //Return error if no discount
    if (discount == undefined) {
      return {
        error: "Invalid Discount!",
      };
    }

    //Finds all the activities
    let entities = await strapi.services.activities.find();

    for (const entity of entities) {
      let id = entity.id;
      let price = entity.price;
      let newPrice = price - (price * discount) / 100;

      //Update price for the activity
      await strapi.services.activities.update(
        { id },
        { price: newPrice.toFixed(2) }
      );
    }

    //Fetch updated entities
    entities = await strapi.services.activities.find();

    //Sanitize the activities
    return entities.map((entity) =>
      sanitizeEntity(entity, { model: strapi.models.activities })
    );
  },
};
