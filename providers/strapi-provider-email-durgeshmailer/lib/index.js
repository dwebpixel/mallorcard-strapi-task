'use strict'

/**
 * Module dependencies
 */

/* eslint-disable import/no-unresolved */
/* eslint-disable prefer-template */
// Public node modules.
const nodemailer = require('nodemailer')
const { removeUndefined } = require('strapi-utils');

/* eslint-disable no-unused-vars */
module.exports = {
  init: (providerOptions = {}, settings = {}) => {

    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
      host: providerOptions.host,
      port: providerOptions.port,
      secure: providerOptions.secure, // true for 465, false for other ports
      auth: {
        user: providerOptions.auth.user, // generated ethereal user
        pass: providerOptions.auth.password, // generated ethereal password
      },
      tls: {
        rejectUnauthorized: providerOptions.rejectUnauthorized
      }
    });

    return {
      send: (options) => {
        return new Promise((resolve, reject) => {
          const { from, to, cc, bcc, replyTo, subject, text, html, ...rest } = options;

          let msg = {
            from: from || settings.defaultFrom,
            to,
            cc,
            bcc,
            replyTo: replyTo || settings.defaultReplyTo,
            subject,
            text,
            html,
            ...rest,
          };

          transporter
            .sendMail(removeUndefined(msg), (err) => {
              if (err) {
                reject(err);
              }
              resolve();
            });
        });
      },
    };
  },
};
