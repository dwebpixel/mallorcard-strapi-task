-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 04, 2020 at 04:38 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mallorcard`
--

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE `activities` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `subtitle` varchar(255) DEFAULT NULL,
  `description` longtext,
  `price` decimal(10,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `activities`
--

INSERT INTO `activities` (`id`, `title`, `subtitle`, `description`, `price`, `created_at`, `updated_at`) VALUES
(1, ' Trekking and Mountaineering ', 'A leisurely day hike or a challenging walk', 'India\'s mountain ranges provide plenty of trekking and mountaineering options for all fitness levels, whether you\'d prefer a leisurely day hike or a challenging walk along the frozen Zanskar River (the Chadar Ice Trek). ', '4050.00', '2020-06-04 02:34:33', '2020-06-04 05:00:06'),
(2, 'Rafting and Kayaking ', 'The extra adrenaline rush from whitewater ', 'Going on a rafting expedition in India combines picturesque scenery with exciting thrills and spills. Raft down renowned rivers, such as the holy Ganges, by day and camp at pristine beaches by night. If you have a real adventure on your mind, there\'s nothing like the extra adrenaline rush from whitewater kayaking. ', '4500.00', '2020-06-04 04:51:57', '2020-06-04 05:00:07'),
(3, 'Paragliding', 'Fly like a bird', 'If you\'ve ever wondered what it\'s like to fly like a bird, paragliding is the answer! There are two ways to go about it -- learn how to do it yourself or take a tandem joyride with a professional', '3420.00', '2020-06-04 04:55:52', '2020-06-04 05:00:07'),
(6, 'Motorcycle Touring', 'Love of traveling and motorbike riding', 'Touring India on a motorcycle is the ultimate way of exploring the countryside if you have a love of traveling and motorbike riding. Enjoy the freedom to discover and experience India in a way which isn\'t possible using other forms of transport.', '3000.00', '2020-06-04 05:25:35', '2020-06-04 05:25:35'),
(7, 'Motorcycle Touring', 'Love of traveling and motorbike riding testing', 'Touring India on a motorcycle is the ultimate way of exploring the countryside if you have a love of traveling and motorbike riding. Enjoy the freedom to discover and experience India in a way which isn\'t possible using other forms of transport.', '3000.00', '2020-06-04 05:28:34', '2020-06-04 05:28:34');

-- --------------------------------------------------------

--
-- Table structure for table `activities_components`
--

CREATE TABLE `activities_components` (
  `id` int(10) UNSIGNED NOT NULL,
  `field` varchar(255) NOT NULL,
  `order` int(10) UNSIGNED NOT NULL,
  `component_type` varchar(255) NOT NULL,
  `component_id` int(11) NOT NULL,
  `activity_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `activities_components`
--

INSERT INTO `activities_components` (`id`, `field`, `order`, `component_type`, `component_id`, `activity_id`) VALUES
(1, 'german_translation', 1, 'components_internationalization_germen', 1, 1),
(2, 'spanish_translation', 1, 'components_internationalization_spanishes', 1, 1),
(3, 'german_translation', 1, 'components_internationalization_germen', 2, 2),
(4, 'spanish_translation', 1, 'components_internationalization_spanishes', 2, 2),
(5, 'german_translation', 1, 'components_internationalization_germen', 3, 3),
(6, 'spanish_translation', 1, 'components_internationalization_spanishes', 3, 3),
(7, 'german_translation', 1, 'components_internationalization_germen', 4, 6),
(8, 'spanish_translation', 1, 'components_internationalization_spanishes', 4, 6),
(9, 'german_translation', 1, 'components_internationalization_germen', 5, 7),
(10, 'spanish_translation', 1, 'components_internationalization_spanishes', 5, 7);

-- --------------------------------------------------------

--
-- Table structure for table `components_internationalization_germen`
--

CREATE TABLE `components_internationalization_germen` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `subtitle` varchar(255) DEFAULT NULL,
  `description` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `components_internationalization_germen`
--

INSERT INTO `components_internationalization_germen` (`id`, `title`, `subtitle`, `description`) VALUES
(1, 'Trekking und Bergsteigen', 'Eine gemütliche Tageswanderung oder ein herausfordernder Spaziergang', 'India\'s Gebirgszüge bieten zahlreiche Möglichkeiten zum Wandern und Bergsteigen für alle Fitnessstufen, egal ob Sie eine gemütliche Tageswanderung oder einen herausfordernden Spaziergang entlang des gefrorenen Zanskar-Flusses (Chadar Ice Trek) bevorzugen.'),
(2, 'Rafting und Kajakfahren', 'Der zusätzliche Adrenalinschub aus Wildwasser', 'Eine Rafting-Expedition in Indien kombiniert malerische Landschaften mit aufregendem Nervenkitzel und Nervenkitzel. Raften Sie tagsüber auf bekannten Flüssen wie dem heiligen Ganges und campen Sie nachts an unberührten Stränden. Wenn Sie ein echtes Abenteuer im Kopf haben, gibt es nichts Schöneres als den zusätzlichen Adrenalinschub beim Wildwasserkajakfahren.'),
(3, 'Gleitschirmfliegen', 'Fliege wie ein Vogel', 'Wenn Sie sich jemals gefragt haben, wie es ist, wie ein Vogel zu fliegen, ist Paragliding die Antwort! Es gibt zwei Möglichkeiten: Lernen Sie, wie man es selbst macht, oder machen Sie eine Tandem-Spritztour mit einem Profi'),
(4, 'Touring en moto', 'Liebe zum Reisen und Motorradfahren', 'Eine Motorradtour durch Indien ist die ultimative Art, die Landschaft zu erkunden, wenn Sie gerne reisen und Motorrad fahren. Genießen Sie die Freiheit, Indien auf eine Weise zu entdecken und zu erleben, die mit anderen Verkehrsmitteln nicht möglich ist.'),
(5, 'Touring en moto', 'Liebe zum Reisen und Motorradfahren', 'Eine Motorradtour durch Indien ist die ultimative Art, die Landschaft zu erkunden, wenn Sie gerne reisen und Motorrad fahren. Genießen Sie die Freiheit, Indien auf eine Weise zu entdecken und zu erleben, die mit anderen Verkehrsmitteln nicht möglich ist.');

-- --------------------------------------------------------

--
-- Table structure for table `components_internationalization_spanishes`
--

CREATE TABLE `components_internationalization_spanishes` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `subtitle` varchar(255) DEFAULT NULL,
  `description` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `components_internationalization_spanishes`
--

INSERT INTO `components_internationalization_spanishes` (`id`, `title`, `subtitle`, `description`) VALUES
(1, 'Trekking y Montañismo', 'Una caminata de un día o una caminata desafiante', 'Las cadenas montañosas de la India ofrecen muchas opciones de trekking y montañismo para todos los niveles de condición física, ya sea que prefiera una caminata de un día o una caminata desafiante a lo largo del helado río Zanskar (el Chadar Ice Trek).'),
(2, 'Rafting y Kayak', 'La adrenalina extra de las aguas bravas', 'Ir a una expedición de rafting en la India combina paisajes pintorescos con emocionantes emociones y derrames. Navegue en balsa por ríos famosos, como el sagrado Ganges, durante el día y acampe en las playas vírgenes por la noche. Si tienes una aventura real en mente, no hay nada como la adrenalina extra del kayak de aguas bravas.'),
(3, 'Parapente', 'Vuela como un pájaro', 'Si alguna vez te has preguntado cómo es volar como un pájaro, ¡el parapente es la respuesta! Hay dos formas de hacerlo: aprenda cómo hacerlo usted mismo o tome un viaje en tándem con un profesional'),
(4, 'Trekking y Montañismo', 'Amor por viajar y andar en moto', 'Viajar por la India en motocicleta es la mejor manera de explorar el campo si te encanta viajar y andar en moto. Disfrute de la libertad de descubrir y experimentar la India de una manera que no es posible con otras formas de transporte.'),
(5, 'Trekking y Montañismo', 'Amor por viajar y andar en moto', 'Viajar por la India en motocicleta es la mejor manera de explorar el campo si te encanta viajar y andar en moto. Disfrute de la libertad de descubrir y experimentar la India de una manera que no es posible con otras formas de transporte.');

-- --------------------------------------------------------

--
-- Table structure for table `core_store`
--

CREATE TABLE `core_store` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) DEFAULT NULL,
  `value` longtext,
  `type` varchar(255) DEFAULT NULL,
  `environment` varchar(255) DEFAULT NULL,
  `tag` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `core_store`
--

INSERT INTO `core_store` (`id`, `key`, `value`, `type`, `environment`, `tag`) VALUES
(1, 'db_model_strapi_webhooks', '{\"name\":{\"type\":\"string\"},\"url\":{\"type\":\"text\"},\"headers\":{\"type\":\"json\"},\"events\":{\"type\":\"json\"},\"enabled\":{\"type\":\"boolean\"}}', 'object', NULL, NULL),
(2, 'db_model_upload_file', '{\"name\":{\"type\":\"string\",\"configurable\":false,\"required\":true},\"alternativeText\":{\"type\":\"string\",\"configurable\":false},\"caption\":{\"type\":\"string\",\"configurable\":false},\"width\":{\"type\":\"integer\",\"configurable\":false},\"height\":{\"type\":\"integer\",\"configurable\":false},\"formats\":{\"type\":\"json\",\"configurable\":false},\"hash\":{\"type\":\"string\",\"configurable\":false,\"required\":true},\"ext\":{\"type\":\"string\",\"configurable\":false},\"mime\":{\"type\":\"string\",\"configurable\":false,\"required\":true},\"size\":{\"type\":\"decimal\",\"configurable\":false,\"required\":true},\"url\":{\"type\":\"string\",\"configurable\":false,\"required\":true},\"previewUrl\":{\"type\":\"string\",\"configurable\":false},\"provider\":{\"type\":\"string\",\"configurable\":false,\"required\":true},\"provider_metadata\":{\"type\":\"json\",\"configurable\":false},\"related\":{\"collection\":\"*\",\"filter\":\"field\",\"configurable\":false},\"created_at\":{\"type\":\"currentTimestamp\"},\"updated_at\":{\"type\":\"currentTimestamp\"}}', 'object', NULL, NULL),
(3, 'db_model_users-permissions_permission', '{\"type\":{\"type\":\"string\",\"required\":true,\"configurable\":false},\"controller\":{\"type\":\"string\",\"required\":true,\"configurable\":false},\"action\":{\"type\":\"string\",\"required\":true,\"configurable\":false},\"enabled\":{\"type\":\"boolean\",\"required\":true,\"configurable\":false},\"policy\":{\"type\":\"string\",\"configurable\":false},\"role\":{\"model\":\"role\",\"via\":\"permissions\",\"plugin\":\"users-permissions\",\"configurable\":false}}', 'object', NULL, NULL),
(4, 'db_model_core_store', '{\"key\":{\"type\":\"string\"},\"value\":{\"type\":\"text\"},\"type\":{\"type\":\"string\"},\"environment\":{\"type\":\"string\"},\"tag\":{\"type\":\"string\"}}', 'object', NULL, NULL),
(5, 'db_model_upload_file_morph', '{\"upload_file_id\":{\"type\":\"integer\"},\"related_id\":{\"type\":\"integer\"},\"related_type\":{\"type\":\"text\"},\"field\":{\"type\":\"text\"},\"order\":{\"type\":\"integer\"}}', 'object', NULL, NULL),
(6, 'db_model_users-permissions_role', '{\"name\":{\"type\":\"string\",\"minLength\":3,\"required\":true,\"configurable\":false},\"description\":{\"type\":\"string\",\"configurable\":false},\"type\":{\"type\":\"string\",\"unique\":true,\"configurable\":false},\"permissions\":{\"collection\":\"permission\",\"via\":\"role\",\"plugin\":\"users-permissions\",\"configurable\":false,\"isVirtual\":true},\"users\":{\"collection\":\"user\",\"via\":\"role\",\"configurable\":false,\"plugin\":\"users-permissions\",\"isVirtual\":true}}', 'object', NULL, NULL),
(7, 'db_model_strapi_administrator', '{\"username\":{\"type\":\"string\",\"minLength\":3,\"unique\":true,\"configurable\":false,\"required\":true},\"email\":{\"type\":\"email\",\"minLength\":6,\"configurable\":false,\"required\":true},\"password\":{\"type\":\"password\",\"minLength\":6,\"configurable\":false,\"private\":true,\"required\":true},\"resetPasswordToken\":{\"type\":\"string\",\"configurable\":false,\"private\":true},\"blocked\":{\"type\":\"boolean\",\"default\":false,\"configurable\":false}}', 'object', NULL, NULL),
(8, 'db_model_users-permissions_user', '{\"username\":{\"type\":\"string\",\"minLength\":3,\"unique\":true,\"configurable\":false,\"required\":true},\"email\":{\"type\":\"email\",\"minLength\":6,\"configurable\":false,\"required\":true},\"provider\":{\"type\":\"string\",\"configurable\":false},\"password\":{\"type\":\"password\",\"minLength\":6,\"configurable\":false,\"private\":true},\"resetPasswordToken\":{\"type\":\"string\",\"configurable\":false,\"private\":true},\"confirmed\":{\"type\":\"boolean\",\"default\":false,\"configurable\":false},\"blocked\":{\"type\":\"boolean\",\"default\":false,\"configurable\":false},\"role\":{\"model\":\"role\",\"via\":\"users\",\"plugin\":\"users-permissions\",\"configurable\":false},\"created_at\":{\"type\":\"currentTimestamp\"},\"updated_at\":{\"type\":\"currentTimestamp\"}}', 'object', NULL, NULL),
(9, 'plugin_users-permissions_grant', '{\"email\":{\"enabled\":true,\"icon\":\"envelope\"},\"discord\":{\"enabled\":false,\"icon\":\"discord\",\"key\":\"\",\"secret\":\"\",\"callback\":\"/auth/discord/callback\",\"scope\":[\"identify\",\"email\"]},\"facebook\":{\"enabled\":false,\"icon\":\"facebook-square\",\"key\":\"\",\"secret\":\"\",\"callback\":\"/auth/facebook/callback\",\"scope\":[\"email\"]},\"google\":{\"enabled\":false,\"icon\":\"google\",\"key\":\"\",\"secret\":\"\",\"callback\":\"/auth/google/callback\",\"scope\":[\"email\"]},\"github\":{\"enabled\":false,\"icon\":\"github\",\"key\":\"\",\"secret\":\"\",\"callback\":\"/auth/github/callback\",\"scope\":[\"user\",\"user:email\"]},\"microsoft\":{\"enabled\":false,\"icon\":\"windows\",\"key\":\"\",\"secret\":\"\",\"callback\":\"/auth/microsoft/callback\",\"scope\":[\"user.read\"]},\"twitter\":{\"enabled\":false,\"icon\":\"twitter\",\"key\":\"\",\"secret\":\"\",\"callback\":\"/auth/twitter/callback\"},\"instagram\":{\"enabled\":false,\"icon\":\"instagram\",\"key\":\"\",\"secret\":\"\",\"callback\":\"/auth/instagram/callback\"},\"vk\":{\"enabled\":false,\"icon\":\"vk\",\"key\":\"\",\"secret\":\"\",\"callback\":\"/auth/vk/callback\",\"scope\":[\"email\"]},\"twitch\":{\"enabled\":false,\"icon\":\"twitch\",\"key\":\"\",\"secret\":\"\",\"callback\":\"/auth/twitch/callback\",\"scope\":[\"user:read:email\"]}}', 'object', '', ''),
(10, 'plugin_upload_settings', '{\"sizeOptimization\":true,\"responsiveDimensions\":true}', 'object', 'development', ''),
(11, 'plugin_content_manager_configuration_content_types::plugins::users-permissions.permission', '{\"uid\":\"plugins::users-permissions.permission\",\"settings\":{\"bulkable\":true,\"filterable\":true,\"searchable\":true,\"pageSize\":10,\"mainField\":\"type\",\"defaultSortBy\":\"type\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"Id\",\"searchable\":true,\"sortable\":true}},\"type\":{\"edit\":{\"label\":\"Type\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Type\",\"searchable\":true,\"sortable\":true}},\"controller\":{\"edit\":{\"label\":\"Controller\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Controller\",\"searchable\":true,\"sortable\":true}},\"action\":{\"edit\":{\"label\":\"Action\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Action\",\"searchable\":true,\"sortable\":true}},\"enabled\":{\"edit\":{\"label\":\"Enabled\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Enabled\",\"searchable\":true,\"sortable\":true}},\"policy\":{\"edit\":{\"label\":\"Policy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Policy\",\"searchable\":true,\"sortable\":true}},\"role\":{\"edit\":{\"label\":\"Role\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"name\"},\"list\":{\"label\":\"Role\",\"searchable\":false,\"sortable\":false}}},\"layouts\":{\"list\":[\"id\",\"type\",\"controller\",\"action\"],\"editRelations\":[\"role\"],\"edit\":[[{\"name\":\"type\",\"size\":6},{\"name\":\"controller\",\"size\":6}],[{\"name\":\"action\",\"size\":6},{\"name\":\"enabled\",\"size\":4}],[{\"name\":\"policy\",\"size\":6}]]}}', 'object', '', ''),
(12, 'plugin_content_manager_configuration_content_types::plugins::users-permissions.role', '{\"uid\":\"plugins::users-permissions.role\",\"settings\":{\"bulkable\":true,\"filterable\":true,\"searchable\":true,\"pageSize\":10,\"mainField\":\"name\",\"defaultSortBy\":\"name\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"Id\",\"searchable\":true,\"sortable\":true}},\"name\":{\"edit\":{\"label\":\"Name\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Name\",\"searchable\":true,\"sortable\":true}},\"description\":{\"edit\":{\"label\":\"Description\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Description\",\"searchable\":true,\"sortable\":true}},\"type\":{\"edit\":{\"label\":\"Type\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Type\",\"searchable\":true,\"sortable\":true}},\"permissions\":{\"edit\":{\"label\":\"Permissions\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"type\"},\"list\":{\"label\":\"Permissions\",\"searchable\":false,\"sortable\":false}},\"users\":{\"edit\":{\"label\":\"Users\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"username\"},\"list\":{\"label\":\"Users\",\"searchable\":false,\"sortable\":false}}},\"layouts\":{\"list\":[\"id\",\"name\",\"description\",\"type\"],\"editRelations\":[\"permissions\",\"users\"],\"edit\":[[{\"name\":\"name\",\"size\":6},{\"name\":\"description\",\"size\":6}],[{\"name\":\"type\",\"size\":6}]]}}', 'object', '', ''),
(13, 'plugin_content_manager_configuration_content_types::plugins::upload.file', '{\"uid\":\"plugins::upload.file\",\"settings\":{\"bulkable\":true,\"filterable\":true,\"searchable\":true,\"pageSize\":10,\"mainField\":\"name\",\"defaultSortBy\":\"name\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"Id\",\"searchable\":true,\"sortable\":true}},\"name\":{\"edit\":{\"label\":\"Name\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Name\",\"searchable\":true,\"sortable\":true}},\"alternativeText\":{\"edit\":{\"label\":\"AlternativeText\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"AlternativeText\",\"searchable\":true,\"sortable\":true}},\"caption\":{\"edit\":{\"label\":\"Caption\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Caption\",\"searchable\":true,\"sortable\":true}},\"width\":{\"edit\":{\"label\":\"Width\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Width\",\"searchable\":true,\"sortable\":true}},\"height\":{\"edit\":{\"label\":\"Height\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Height\",\"searchable\":true,\"sortable\":true}},\"formats\":{\"edit\":{\"label\":\"Formats\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Formats\",\"searchable\":false,\"sortable\":false}},\"hash\":{\"edit\":{\"label\":\"Hash\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Hash\",\"searchable\":true,\"sortable\":true}},\"ext\":{\"edit\":{\"label\":\"Ext\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Ext\",\"searchable\":true,\"sortable\":true}},\"mime\":{\"edit\":{\"label\":\"Mime\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Mime\",\"searchable\":true,\"sortable\":true}},\"size\":{\"edit\":{\"label\":\"Size\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Size\",\"searchable\":true,\"sortable\":true}},\"url\":{\"edit\":{\"label\":\"Url\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Url\",\"searchable\":true,\"sortable\":true}},\"previewUrl\":{\"edit\":{\"label\":\"PreviewUrl\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"PreviewUrl\",\"searchable\":true,\"sortable\":true}},\"provider\":{\"edit\":{\"label\":\"Provider\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Provider\",\"searchable\":true,\"sortable\":true}},\"provider_metadata\":{\"edit\":{\"label\":\"Provider_metadata\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Provider_metadata\",\"searchable\":false,\"sortable\":false}},\"related\":{\"edit\":{\"label\":\"Related\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"id\"},\"list\":{\"label\":\"Related\",\"searchable\":false,\"sortable\":false}},\"created_at\":{\"edit\":{\"label\":\"Created_at\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"Created_at\",\"searchable\":true,\"sortable\":true}},\"updated_at\":{\"edit\":{\"label\":\"Updated_at\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"Updated_at\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"name\",\"alternativeText\",\"caption\"],\"editRelations\":[\"related\"],\"edit\":[[{\"name\":\"name\",\"size\":6},{\"name\":\"alternativeText\",\"size\":6}],[{\"name\":\"caption\",\"size\":6},{\"name\":\"width\",\"size\":4}],[{\"name\":\"height\",\"size\":4}],[{\"name\":\"formats\",\"size\":12}],[{\"name\":\"hash\",\"size\":6},{\"name\":\"ext\",\"size\":6}],[{\"name\":\"mime\",\"size\":6},{\"name\":\"size\",\"size\":4}],[{\"name\":\"url\",\"size\":6},{\"name\":\"previewUrl\",\"size\":6}],[{\"name\":\"provider\",\"size\":6}],[{\"name\":\"provider_metadata\",\"size\":12}]]}}', 'object', '', ''),
(14, 'plugin_content_manager_configuration_content_types::strapi::administrator', '{\"uid\":\"strapi::administrator\",\"settings\":{\"bulkable\":true,\"filterable\":true,\"searchable\":true,\"pageSize\":10,\"mainField\":\"username\",\"defaultSortBy\":\"username\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"Id\",\"searchable\":true,\"sortable\":true}},\"username\":{\"edit\":{\"label\":\"Username\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Username\",\"searchable\":true,\"sortable\":true}},\"email\":{\"edit\":{\"label\":\"Email\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Email\",\"searchable\":true,\"sortable\":true}},\"password\":{\"edit\":{\"label\":\"Password\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Password\",\"searchable\":true,\"sortable\":true}},\"resetPasswordToken\":{\"edit\":{\"label\":\"ResetPasswordToken\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"ResetPasswordToken\",\"searchable\":true,\"sortable\":true}},\"blocked\":{\"edit\":{\"label\":\"Blocked\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Blocked\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"username\",\"email\",\"blocked\"],\"editRelations\":[],\"edit\":[[{\"name\":\"username\",\"size\":6},{\"name\":\"email\",\"size\":6}],[{\"name\":\"password\",\"size\":6},{\"name\":\"blocked\",\"size\":4}]]}}', 'object', '', ''),
(15, 'plugin_content_manager_configuration_content_types::plugins::users-permissions.user', '{\"uid\":\"plugins::users-permissions.user\",\"settings\":{\"bulkable\":true,\"filterable\":true,\"searchable\":true,\"pageSize\":10,\"mainField\":\"username\",\"defaultSortBy\":\"username\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"Id\",\"searchable\":true,\"sortable\":true}},\"username\":{\"edit\":{\"label\":\"Username\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Username\",\"searchable\":true,\"sortable\":true}},\"email\":{\"edit\":{\"label\":\"Email\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Email\",\"searchable\":true,\"sortable\":true}},\"provider\":{\"edit\":{\"label\":\"Provider\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"Provider\",\"searchable\":true,\"sortable\":true}},\"password\":{\"edit\":{\"label\":\"Password\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Password\",\"searchable\":true,\"sortable\":true}},\"resetPasswordToken\":{\"edit\":{\"label\":\"ResetPasswordToken\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"ResetPasswordToken\",\"searchable\":true,\"sortable\":true}},\"confirmed\":{\"edit\":{\"label\":\"Confirmed\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Confirmed\",\"searchable\":true,\"sortable\":true}},\"blocked\":{\"edit\":{\"label\":\"Blocked\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Blocked\",\"searchable\":true,\"sortable\":true}},\"role\":{\"edit\":{\"label\":\"Role\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"name\"},\"list\":{\"label\":\"Role\",\"searchable\":false,\"sortable\":false}},\"created_at\":{\"edit\":{\"label\":\"Created_at\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"Created_at\",\"searchable\":true,\"sortable\":true}},\"updated_at\":{\"edit\":{\"label\":\"Updated_at\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"Updated_at\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"username\",\"email\",\"confirmed\"],\"editRelations\":[\"role\"],\"edit\":[[{\"name\":\"username\",\"size\":6},{\"name\":\"email\",\"size\":6}],[{\"name\":\"password\",\"size\":6},{\"name\":\"confirmed\",\"size\":4}],[{\"name\":\"blocked\",\"size\":4}]]}}', 'object', '', ''),
(16, 'plugin_users-permissions_email', '{\"reset_password\":{\"display\":\"Email.template.reset_password\",\"icon\":\"sync\",\"options\":{\"from\":{\"name\":\"Administration Panel\",\"email\":\"no-reply@strapi.io\"},\"response_email\":\"\",\"object\":\"Reset password\",\"message\":\"<p>We heard that you lost your password. Sorry about that!</p>\\n\\n<p>But don’t worry! You can use the following link to reset your password:</p>\\n\\n<p><%= URL %>?code=<%= TOKEN %></p>\\n\\n<p>Thanks.</p>\"}},\"email_confirmation\":{\"display\":\"Email.template.email_confirmation\",\"icon\":\"check-square\",\"options\":{\"from\":{\"name\":\"Administration Panel\",\"email\":\"no-reply@strapi.io\"},\"response_email\":\"\",\"object\":\"Account confirmation\",\"message\":\"<p>Thank you for registering!</p>\\n\\n<p>You have to confirm your email address. Please click on the link below.</p>\\n\\n<p><%= URL %>?confirmation=<%= CODE %></p>\\n\\n<p>Thanks.</p>\"}}}', 'object', '', ''),
(17, 'plugin_users-permissions_advanced', '{\"unique_email\":true,\"allow_register\":true,\"email_confirmation\":false,\"email_confirmation_redirection\":\"/admin/admin\",\"email_reset_password\":\"/admin/admin\",\"default_role\":\"authenticated\"}', 'object', '', ''),
(18, 'db_model_activities', '{\"title\":{\"type\":\"string\",\"required\":true},\"subtitle\":{\"type\":\"string\",\"required\":false},\"description\":{\"type\":\"text\"},\"price\":{\"type\":\"decimal\"},\"pictures\":{\"collection\":\"file\",\"via\":\"related\",\"allowedTypes\":[\"images\",\"files\",\"videos\"],\"plugin\":\"upload\",\"required\":false},\"german_translation\":{\"type\":\"component\",\"repeatable\":false,\"component\":\"internationalization.german\"},\"spanish_translation\":{\"type\":\"component\",\"repeatable\":false,\"component\":\"internationalization.spanish\"},\"created_at\":{\"type\":\"currentTimestamp\"},\"updated_at\":{\"type\":\"currentTimestamp\"}}', 'object', NULL, NULL),
(19, 'plugin_content_manager_configuration_content_types::application::activities.activities', '{\"uid\":\"application::activities.activities\",\"settings\":{\"bulkable\":true,\"filterable\":true,\"searchable\":true,\"pageSize\":10,\"mainField\":\"title\",\"defaultSortBy\":\"title\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"Id\",\"searchable\":true,\"sortable\":true}},\"title\":{\"edit\":{\"label\":\"Title\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Title\",\"searchable\":true,\"sortable\":true}},\"subtitle\":{\"edit\":{\"label\":\"Subtitle\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Subtitle\",\"searchable\":true,\"sortable\":true}},\"description\":{\"edit\":{\"label\":\"Description\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Description\",\"searchable\":true,\"sortable\":true}},\"price\":{\"edit\":{\"label\":\"Price\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Price\",\"searchable\":true,\"sortable\":true}},\"pictures\":{\"edit\":{\"label\":\"Pictures\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Pictures\",\"searchable\":false,\"sortable\":false}},\"german_translation\":{\"edit\":{\"label\":\"German_translation\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"German_translation\",\"searchable\":false,\"sortable\":false}},\"spanish_translation\":{\"edit\":{\"label\":\"Spanish_translation\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Spanish_translation\",\"searchable\":false,\"sortable\":false}},\"created_at\":{\"edit\":{\"label\":\"Created_at\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"Created_at\",\"searchable\":true,\"sortable\":true}},\"updated_at\":{\"edit\":{\"label\":\"Updated_at\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"Updated_at\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"title\",\"subtitle\",\"description\"],\"edit\":[[{\"name\":\"title\",\"size\":6},{\"name\":\"subtitle\",\"size\":6}],[{\"name\":\"description\",\"size\":6},{\"name\":\"price\",\"size\":4}],[{\"name\":\"pictures\",\"size\":6}],[{\"name\":\"german_translation\",\"size\":12}],[{\"name\":\"spanish_translation\",\"size\":12}]],\"editRelations\":[]}}', 'object', '', ''),
(20, 'db_model_components_internationalization_germen', '{\"title\":{\"type\":\"string\"},\"subtitle\":{\"type\":\"string\"},\"description\":{\"type\":\"text\"}}', 'object', NULL, NULL),
(21, 'plugin_content_manager_configuration_components::internationalization.german', '{\"uid\":\"internationalization.german\",\"isComponent\":true,\"settings\":{\"bulkable\":true,\"filterable\":true,\"searchable\":true,\"pageSize\":10,\"mainField\":\"title\",\"defaultSortBy\":\"title\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"Id\",\"searchable\":false,\"sortable\":false}},\"title\":{\"edit\":{\"label\":\"Title\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Title\",\"searchable\":true,\"sortable\":true}},\"subtitle\":{\"edit\":{\"label\":\"Subtitle\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Subtitle\",\"searchable\":true,\"sortable\":true}},\"description\":{\"edit\":{\"label\":\"Description\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Description\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"title\",\"subtitle\",\"description\"],\"edit\":[[{\"name\":\"title\",\"size\":6},{\"name\":\"subtitle\",\"size\":6}],[{\"name\":\"description\",\"size\":6}]],\"editRelations\":[]}}', 'object', '', ''),
(22, 'db_model_components_internationalization_spanishes', '{\"title\":{\"type\":\"string\"},\"subtitle\":{\"type\":\"string\"},\"description\":{\"type\":\"text\"}}', 'object', NULL, NULL),
(23, 'plugin_content_manager_configuration_components::internationalization.spanish', '{\"uid\":\"internationalization.spanish\",\"isComponent\":true,\"settings\":{\"bulkable\":true,\"filterable\":true,\"searchable\":true,\"pageSize\":10,\"mainField\":\"title\",\"defaultSortBy\":\"title\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"Id\",\"searchable\":false,\"sortable\":false}},\"title\":{\"edit\":{\"label\":\"Title\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Title\",\"searchable\":true,\"sortable\":true}},\"subtitle\":{\"edit\":{\"label\":\"Subtitle\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Subtitle\",\"searchable\":true,\"sortable\":true}},\"description\":{\"edit\":{\"label\":\"Description\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Description\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"title\",\"subtitle\",\"description\"],\"edit\":[[{\"name\":\"title\",\"size\":6},{\"name\":\"subtitle\",\"size\":6}],[{\"name\":\"description\",\"size\":6}]],\"editRelations\":[]}}', 'object', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `strapi_administrator`
--

CREATE TABLE `strapi_administrator` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `resetPasswordToken` varchar(255) DEFAULT NULL,
  `blocked` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `strapi_administrator`
--

INSERT INTO `strapi_administrator` (`id`, `username`, `email`, `password`, `resetPasswordToken`, `blocked`) VALUES
(1, 'Durgesh', 'dwebsoftguy@gmail.com', '$2a$10$LFlL2PPCD1MGzT9m6Eg7geLHu9O/lJ68GOZfsc2hhTKOZ7FjXrOgC', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `strapi_webhooks`
--

CREATE TABLE `strapi_webhooks` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `url` longtext,
  `headers` longtext,
  `events` longtext,
  `enabled` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `upload_file`
--

CREATE TABLE `upload_file` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `alternativeText` varchar(255) DEFAULT NULL,
  `caption` varchar(255) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `formats` longtext,
  `hash` varchar(255) NOT NULL,
  `ext` varchar(255) DEFAULT NULL,
  `mime` varchar(255) NOT NULL,
  `size` decimal(10,2) NOT NULL,
  `url` varchar(255) NOT NULL,
  `previewUrl` varchar(255) DEFAULT NULL,
  `provider` varchar(255) NOT NULL,
  `provider_metadata` longtext,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `upload_file`
--

INSERT INTO `upload_file` (`id`, `name`, `alternativeText`, `caption`, `width`, `height`, `formats`, `hash`, `ext`, `mime`, `size`, `url`, `previewUrl`, `provider`, `provider_metadata`, `created_at`, `updated_at`) VALUES
(1, 'trecking-2', '', '', 292, 172, '{\"thumbnail\":{\"hash\":\"thumbnail_trecking_2_5ced4ecc26\",\"ext\":\".jpeg\",\"mime\":\"image/jpeg\",\"width\":245,\"height\":144,\"size\":9.89,\"path\":null,\"url\":\"/uploads/thumbnail_trecking_2_5ced4ecc26.jpeg\"}}', 'trecking_2_5ced4ecc26', '.jpeg', 'image/jpeg', '12.39', '/uploads/trecking_2_5ced4ecc26.jpeg', NULL, 'local', NULL, '2020-06-04 02:34:17', '2020-06-04 02:34:17'),
(2, 'trekking', '', '', 512, 341, '{\"thumbnail\":{\"hash\":\"thumbnail_trekking_f8b8569d2e\",\"ext\":\".jpeg\",\"mime\":\"image/jpeg\",\"width\":234,\"height\":156,\"size\":11,\"path\":null,\"url\":\"/uploads/thumbnail_trekking_f8b8569d2e.jpeg\"},\"small\":{\"hash\":\"small_trekking_f8b8569d2e\",\"ext\":\".jpeg\",\"mime\":\"image/jpeg\",\"width\":500,\"height\":333,\"size\":41.76,\"path\":null,\"url\":\"/uploads/small_trekking_f8b8569d2e.jpeg\"}}', 'trekking_f8b8569d2e', '.jpeg', 'image/jpeg', '44.20', '/uploads/trekking_f8b8569d2e.jpeg', NULL, 'local', NULL, '2020-06-04 02:34:17', '2020-06-04 02:34:17'),
(3, 'Rafting', '', '', 700, 410, '{\"thumbnail\":{\"hash\":\"thumbnail_Rafting_bf4fa416b8\",\"ext\":\".jpeg\",\"mime\":\"image/jpeg\",\"width\":245,\"height\":144,\"size\":10.6,\"path\":null,\"url\":\"/uploads/thumbnail_Rafting_bf4fa416b8.jpeg\"},\"small\":{\"hash\":\"small_Rafting_bf4fa416b8\",\"ext\":\".jpeg\",\"mime\":\"image/jpeg\",\"width\":500,\"height\":293,\"size\":34.9,\"path\":null,\"url\":\"/uploads/small_Rafting_bf4fa416b8.jpeg\"}}', 'Rafting_bf4fa416b8', '.jpeg', 'image/jpeg', '62.12', '/uploads/Rafting_bf4fa416b8.jpeg', NULL, 'local', NULL, '2020-06-04 04:50:25', '2020-06-04 04:50:25'),
(4, 'paragliding-1', '', '', 400, 300, '{\"thumbnail\":{\"hash\":\"thumbnail_paragliding_1_4e0ccf5ade\",\"ext\":\".jpeg\",\"mime\":\"image/jpeg\",\"width\":208,\"height\":156,\"size\":6.11,\"path\":null,\"url\":\"/uploads/thumbnail_paragliding_1_4e0ccf5ade.jpeg\"}}', 'paragliding_1_4e0ccf5ade', '.jpeg', 'image/jpeg', '17.87', '/uploads/paragliding_1_4e0ccf5ade.jpeg', NULL, 'local', NULL, '2020-06-04 04:54:05', '2020-06-04 04:54:05');

-- --------------------------------------------------------

--
-- Table structure for table `upload_file_morph`
--

CREATE TABLE `upload_file_morph` (
  `id` int(10) UNSIGNED NOT NULL,
  `upload_file_id` int(11) DEFAULT NULL,
  `related_id` int(11) DEFAULT NULL,
  `related_type` longtext,
  `field` longtext,
  `order` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `upload_file_morph`
--

INSERT INTO `upload_file_morph` (`id`, `upload_file_id`, `related_id`, `related_type`, `field`, `order`) VALUES
(3, 1, 1, 'activities', 'pictures', 1),
(4, 2, 1, 'activities', 'pictures', 2),
(5, 3, 2, 'activities', 'pictures', 1),
(6, 4, 3, 'activities', 'pictures', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users-permissions_permission`
--

CREATE TABLE `users-permissions_permission` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(255) NOT NULL,
  `controller` varchar(255) NOT NULL,
  `action` varchar(255) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `policy` varchar(255) DEFAULT NULL,
  `role` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users-permissions_permission`
--

INSERT INTO `users-permissions_permission` (`id`, `type`, `controller`, `action`, `enabled`, `policy`, `role`) VALUES
(1, 'content-manager', 'components', 'findcomponent', 0, '', 1),
(2, 'content-manager', 'contentmanager', 'checkuidavailability', 0, '', 1),
(3, 'content-manager', 'components', 'updatecomponent', 0, '', 2),
(4, 'content-manager', 'components', 'updatecomponent', 0, '', 1),
(5, 'content-manager', 'components', 'findcomponent', 0, '', 2),
(6, 'content-manager', 'components', 'listcomponents', 0, '', 2),
(7, 'content-manager', 'components', 'listcomponents', 0, '', 1),
(8, 'content-manager', 'contentmanager', 'checkuidavailability', 0, '', 2),
(9, 'content-manager', 'contentmanager', 'count', 0, '', 1),
(10, 'content-manager', 'contentmanager', 'count', 0, '', 2),
(11, 'content-manager', 'contentmanager', 'create', 0, '', 1),
(12, 'content-manager', 'contentmanager', 'create', 0, '', 2),
(13, 'content-manager', 'contentmanager', 'delete', 0, '', 1),
(14, 'content-manager', 'contentmanager', 'delete', 0, '', 2),
(15, 'content-manager', 'contentmanager', 'deletemany', 0, '', 1),
(16, 'content-manager', 'contentmanager', 'deletemany', 0, '', 2),
(17, 'content-manager', 'contentmanager', 'find', 0, '', 1),
(18, 'content-manager', 'contentmanager', 'find', 0, '', 2),
(19, 'content-manager', 'contentmanager', 'findone', 0, '', 1),
(20, 'content-manager', 'contentmanager', 'findone', 0, '', 2),
(21, 'content-manager', 'contentmanager', 'generateuid', 0, '', 1),
(22, 'content-manager', 'contentmanager', 'generateuid', 0, '', 2),
(23, 'content-manager', 'contentmanager', 'update', 0, '', 1),
(24, 'content-manager', 'contentmanager', 'update', 0, '', 2),
(25, 'content-manager', 'contenttypes', 'findcontenttype', 0, '', 1),
(26, 'content-manager', 'contenttypes', 'findcontenttype', 0, '', 2),
(27, 'content-manager', 'contenttypes', 'listcontenttypes', 0, '', 1),
(28, 'content-manager', 'contenttypes', 'listcontenttypes', 0, '', 2),
(29, 'content-manager', 'contenttypes', 'updatecontenttype', 0, '', 1),
(30, 'content-manager', 'contenttypes', 'updatecontenttype', 0, '', 2),
(31, 'content-type-builder', 'builder', 'getreservednames', 0, '', 1),
(32, 'content-type-builder', 'builder', 'getreservednames', 0, '', 2),
(33, 'content-type-builder', 'componentcategories', 'deletecategory', 0, '', 1),
(34, 'content-type-builder', 'componentcategories', 'deletecategory', 0, '', 2),
(35, 'content-type-builder', 'componentcategories', 'editcategory', 0, '', 1),
(36, 'content-type-builder', 'componentcategories', 'editcategory', 0, '', 2),
(37, 'content-type-builder', 'components', 'createcomponent', 0, '', 1),
(38, 'content-type-builder', 'components', 'createcomponent', 0, '', 2),
(39, 'content-type-builder', 'components', 'deletecomponent', 0, '', 1),
(40, 'content-type-builder', 'components', 'deletecomponent', 0, '', 2),
(41, 'content-type-builder', 'components', 'getcomponent', 0, '', 1),
(42, 'content-type-builder', 'components', 'getcomponent', 0, '', 2),
(43, 'content-type-builder', 'components', 'getcomponents', 0, '', 1),
(44, 'content-type-builder', 'components', 'getcomponents', 0, '', 2),
(45, 'content-type-builder', 'components', 'updatecomponent', 0, '', 1),
(46, 'content-type-builder', 'components', 'updatecomponent', 0, '', 2),
(47, 'content-type-builder', 'connections', 'getconnections', 0, '', 1),
(48, 'content-type-builder', 'connections', 'getconnections', 0, '', 2),
(49, 'content-type-builder', 'contenttypes', 'createcontenttype', 0, '', 1),
(50, 'content-type-builder', 'contenttypes', 'createcontenttype', 0, '', 2),
(51, 'content-type-builder', 'contenttypes', 'deletecontenttype', 0, '', 1),
(52, 'content-type-builder', 'contenttypes', 'deletecontenttype', 0, '', 2),
(53, 'content-type-builder', 'contenttypes', 'getcontenttype', 0, '', 1),
(54, 'content-type-builder', 'contenttypes', 'getcontenttype', 0, '', 2),
(55, 'content-type-builder', 'contenttypes', 'getcontenttypes', 0, '', 1),
(56, 'content-type-builder', 'contenttypes', 'getcontenttypes', 0, '', 2),
(57, 'content-type-builder', 'contenttypes', 'updatecontenttype', 0, '', 1),
(58, 'content-type-builder', 'contenttypes', 'updatecontenttype', 0, '', 2),
(59, 'email', 'email', 'send', 0, '', 1),
(60, 'email', 'email', 'send', 0, '', 2),
(61, 'upload', 'proxy', 'uploadproxy', 0, '', 1),
(62, 'upload', 'proxy', 'uploadproxy', 0, '', 2),
(63, 'upload', 'upload', 'count', 0, '', 1),
(64, 'upload', 'upload', 'count', 0, '', 2),
(65, 'upload', 'upload', 'destroy', 0, '', 1),
(66, 'upload', 'upload', 'destroy', 0, '', 2),
(67, 'upload', 'upload', 'find', 0, '', 1),
(68, 'upload', 'upload', 'find', 0, '', 2),
(69, 'upload', 'upload', 'findone', 0, '', 1),
(70, 'upload', 'upload', 'findone', 0, '', 2),
(71, 'upload', 'upload', 'getsettings', 0, '', 1),
(72, 'upload', 'upload', 'getsettings', 0, '', 2),
(73, 'upload', 'upload', 'search', 0, '', 1),
(74, 'upload', 'upload', 'search', 0, '', 2),
(75, 'upload', 'upload', 'updatesettings', 0, '', 1),
(76, 'upload', 'upload', 'updatesettings', 0, '', 2),
(77, 'upload', 'upload', 'upload', 0, '', 1),
(78, 'upload', 'upload', 'upload', 0, '', 2),
(79, 'users-permissions', 'auth', 'callback', 0, '', 1),
(80, 'users-permissions', 'auth', 'callback', 1, '', 2),
(81, 'users-permissions', 'auth', 'connect', 1, '', 1),
(82, 'users-permissions', 'auth', 'connect', 1, '', 2),
(83, 'users-permissions', 'auth', 'emailconfirmation', 0, '', 1),
(84, 'users-permissions', 'auth', 'emailconfirmation', 1, '', 2),
(85, 'users-permissions', 'auth', 'forgotpassword', 0, '', 1),
(86, 'users-permissions', 'auth', 'forgotpassword', 1, '', 2),
(87, 'users-permissions', 'auth', 'register', 0, '', 1),
(88, 'users-permissions', 'auth', 'register', 1, '', 2),
(89, 'users-permissions', 'auth', 'resetpassword', 0, '', 1),
(90, 'users-permissions', 'auth', 'resetpassword', 0, '', 2),
(91, 'users-permissions', 'auth', 'sendemailconfirmation', 0, '', 1),
(92, 'users-permissions', 'auth', 'sendemailconfirmation', 0, '', 2),
(93, 'users-permissions', 'user', 'count', 0, '', 1),
(94, 'users-permissions', 'user', 'count', 0, '', 2),
(95, 'users-permissions', 'user', 'create', 0, '', 1),
(96, 'users-permissions', 'user', 'create', 0, '', 2),
(97, 'users-permissions', 'user', 'destroy', 0, '', 1),
(98, 'users-permissions', 'user', 'destroy', 0, '', 2),
(99, 'users-permissions', 'user', 'destroyall', 0, '', 1),
(100, 'users-permissions', 'user', 'destroyall', 0, '', 2),
(101, 'users-permissions', 'user', 'find', 0, '', 1),
(102, 'users-permissions', 'user', 'find', 0, '', 2),
(103, 'users-permissions', 'user', 'findone', 0, '', 1),
(104, 'users-permissions', 'user', 'findone', 0, '', 2),
(105, 'users-permissions', 'user', 'me', 1, '', 1),
(106, 'users-permissions', 'user', 'me', 1, '', 2),
(107, 'users-permissions', 'user', 'update', 0, '', 1),
(108, 'users-permissions', 'user', 'update', 0, '', 2),
(109, 'users-permissions', 'userspermissions', 'createrole', 0, '', 1),
(110, 'users-permissions', 'userspermissions', 'createrole', 0, '', 2),
(111, 'users-permissions', 'userspermissions', 'deleteprovider', 0, '', 1),
(112, 'users-permissions', 'userspermissions', 'deleteprovider', 0, '', 2),
(113, 'users-permissions', 'userspermissions', 'deleterole', 0, '', 1),
(114, 'users-permissions', 'userspermissions', 'deleterole', 0, '', 2),
(115, 'users-permissions', 'userspermissions', 'getadvancedsettings', 0, '', 1),
(116, 'users-permissions', 'userspermissions', 'getadvancedsettings', 0, '', 2),
(117, 'users-permissions', 'userspermissions', 'getemailtemplate', 0, '', 1),
(118, 'users-permissions', 'userspermissions', 'getemailtemplate', 0, '', 2),
(119, 'users-permissions', 'userspermissions', 'getpermissions', 0, '', 1),
(120, 'users-permissions', 'userspermissions', 'getpermissions', 0, '', 2),
(121, 'users-permissions', 'userspermissions', 'getpolicies', 0, '', 1),
(122, 'users-permissions', 'userspermissions', 'getpolicies', 0, '', 2),
(123, 'users-permissions', 'userspermissions', 'getproviders', 0, '', 1),
(124, 'users-permissions', 'userspermissions', 'getproviders', 0, '', 2),
(125, 'users-permissions', 'userspermissions', 'getrole', 0, '', 1),
(126, 'users-permissions', 'userspermissions', 'getrole', 0, '', 2),
(127, 'users-permissions', 'userspermissions', 'getroles', 0, '', 1),
(128, 'users-permissions', 'userspermissions', 'getroles', 0, '', 2),
(129, 'users-permissions', 'userspermissions', 'getroutes', 0, '', 1),
(130, 'users-permissions', 'userspermissions', 'getroutes', 0, '', 2),
(131, 'users-permissions', 'userspermissions', 'index', 0, '', 1),
(132, 'users-permissions', 'userspermissions', 'index', 0, '', 2),
(133, 'users-permissions', 'userspermissions', 'init', 1, '', 1),
(134, 'users-permissions', 'userspermissions', 'init', 1, '', 2),
(135, 'users-permissions', 'userspermissions', 'searchusers', 0, '', 1),
(136, 'users-permissions', 'userspermissions', 'searchusers', 0, '', 2),
(137, 'users-permissions', 'userspermissions', 'updateadvancedsettings', 0, '', 1),
(138, 'users-permissions', 'userspermissions', 'updateadvancedsettings', 0, '', 2),
(139, 'users-permissions', 'userspermissions', 'updateemailtemplate', 0, '', 1),
(140, 'users-permissions', 'userspermissions', 'updateemailtemplate', 0, '', 2),
(141, 'users-permissions', 'userspermissions', 'updateproviders', 0, '', 1),
(142, 'users-permissions', 'userspermissions', 'updateproviders', 0, '', 2),
(143, 'users-permissions', 'userspermissions', 'updaterole', 0, '', 1),
(144, 'users-permissions', 'userspermissions', 'updaterole', 0, '', 2),
(145, 'application', 'activities', 'count', 1, '', 1),
(146, 'application', 'activities', 'find', 1, '', 1),
(147, 'application', 'activities', 'delete', 0, '', 2),
(148, 'application', 'activities', 'count', 1, '', 2),
(149, 'application', 'activities', 'create', 1, '', 2),
(150, 'application', 'activities', 'delete', 1, '', 1),
(151, 'application', 'activities', 'create', 1, '', 1),
(152, 'application', 'activities', 'find', 1, '', 2),
(153, 'application', 'activities', 'findone', 1, '', 1),
(154, 'application', 'activities', 'findone', 1, '', 2),
(155, 'application', 'activities', 'update', 1, '', 1),
(156, 'application', 'activities', 'update', 1, '', 2),
(157, 'application', 'activities', 'updatepricewithdiscount', 1, '', 1),
(158, 'application', 'activities', 'updatepricewithdiscount', 1, '', 2);

-- --------------------------------------------------------

--
-- Table structure for table `users-permissions_role`
--

CREATE TABLE `users-permissions_role` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users-permissions_role`
--

INSERT INTO `users-permissions_role` (`id`, `name`, `description`, `type`) VALUES
(1, 'Authenticated', 'Default role given to authenticated user.', 'authenticated'),
(2, 'Public', 'Default role given to unauthenticated user.', 'public');

-- --------------------------------------------------------

--
-- Table structure for table `users-permissions_user`
--

CREATE TABLE `users-permissions_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `provider` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `resetPasswordToken` varchar(255) DEFAULT NULL,
  `confirmed` tinyint(1) DEFAULT NULL,
  `blocked` tinyint(1) DEFAULT NULL,
  `role` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `activities_components`
--
ALTER TABLE `activities_components`
  ADD PRIMARY KEY (`id`),
  ADD KEY `activity_id_fk` (`activity_id`);

--
-- Indexes for table `components_internationalization_germen`
--
ALTER TABLE `components_internationalization_germen`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `components_internationalization_spanishes`
--
ALTER TABLE `components_internationalization_spanishes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `core_store`
--
ALTER TABLE `core_store`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `strapi_administrator`
--
ALTER TABLE `strapi_administrator`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `strapi_administrator_username_unique` (`username`);

--
-- Indexes for table `strapi_webhooks`
--
ALTER TABLE `strapi_webhooks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `upload_file`
--
ALTER TABLE `upload_file`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `upload_file_morph`
--
ALTER TABLE `upload_file_morph`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users-permissions_permission`
--
ALTER TABLE `users-permissions_permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users-permissions_role`
--
ALTER TABLE `users-permissions_role`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users-permissions_role_type_unique` (`type`);

--
-- Indexes for table `users-permissions_user`
--
ALTER TABLE `users-permissions_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users-permissions_user_username_unique` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `activities_components`
--
ALTER TABLE `activities_components`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `components_internationalization_germen`
--
ALTER TABLE `components_internationalization_germen`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `components_internationalization_spanishes`
--
ALTER TABLE `components_internationalization_spanishes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `core_store`
--
ALTER TABLE `core_store`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `strapi_administrator`
--
ALTER TABLE `strapi_administrator`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `strapi_webhooks`
--
ALTER TABLE `strapi_webhooks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `upload_file`
--
ALTER TABLE `upload_file`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `upload_file_morph`
--
ALTER TABLE `upload_file_morph`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users-permissions_permission`
--
ALTER TABLE `users-permissions_permission`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=159;

--
-- AUTO_INCREMENT for table `users-permissions_role`
--
ALTER TABLE `users-permissions_role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users-permissions_user`
--
ALTER TABLE `users-permissions_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `activities_components`
--
ALTER TABLE `activities_components`
  ADD CONSTRAINT `activity_id_fk` FOREIGN KEY (`activity_id`) REFERENCES `activities` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
